// Решение 1.
    

const lists = document.querySelectorAll('.list-services');

const contents = document.querySelectorAll('.content');

lists.forEach((tab) => {
    tab.addEventListener('click', () => {

        lists.forEach((tab) => {
            tab.classList.remove('active');
        })

        tab.classList.add('active');

        const tabId = tab.getAttribute('data-tab');

        contents.forEach((content) => {
            content.classList.remove('active');

            if(content.id === tabId) {
                content.classList.add('active');
            }
        })
    })
})

// Решение 3.

const categoryLinks = document.querySelectorAll('.whole-list li');

const images = document.querySelectorAll('.pictures-content img');

categoryLinks.forEach((elem) => {
    elem.addEventListener('click', () => {

        categoryLinks.forEach((elem) => {
            elem.classList.remove('active');
        });

        elem.classList.add('active');

        const category = elem.getAttribute('data-category');

        images.forEach((image) => {
            const imgCategory = image.dataset.category;

            if(category === 'all' || category === imgCategory) {
                image.style.display = 'block';
            } else {
                image.style.display = 'none'
            };
        })
    });
})


// Решение 2.

const loadMoreButton = document.querySelector(".btn-load-more");

const picturesContent = document.querySelector(".pictures-content");

const newImages = [
    "./image/our-work/pills.png",
    "./image/our-work/pills.png",
    "./image/our-work/pills.png",
    "./image/our-work/pills.png",
    "./image/our-work/tablet.png",
    "./image/our-work/tablet.png",
    "./image/our-work/tablet.png",
    "./image/our-work/tablet.png",
    "./image/our-work/megapack.png",
    "./image/our-work/megapack.png",
    "./image/our-work/megapack.png",
    "./image/our-work/megapack.png"
];

loadMoreButton.addEventListener("click", () => {
    for(let i = 0; i < newImages.length; i++) {
        let image = document.createElement("img");
        image.src = newImages[i];
        picturesContent.appendChild(image);
    };

    loadMoreButton.style.display = "none";
});